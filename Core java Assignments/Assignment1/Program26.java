/*Q.26.Write a program to find minimum and maximum number as well as 
to add two float numbers using methods of Float.
*/




class Program26{
    public static void main(String[] args) {
        float a = 44.4F;
        float b = 55.5F;
        System.out.println(Float.max(a, b)); 
        System.out.println(Float.min(a, b)); 
        System.out.println(Float.sum(a, b));
    }
}