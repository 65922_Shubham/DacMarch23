/*Q10.Write a program to perform below operations on char type to 
get:
a. The number of bits used to represent a char value
b. The number of bytes used to represent a char value
c. The minimum value a char
d. The maximum value a char
*/

class Program10{
    public static void main(String[] args){
       System.out.println("SIZE "+Character.SIZE);//16
       System.out.println("BYTES "+Character.BYTES);//2
       System.out.println("MAX_VALUE "+Character.MAX_VALUE);//
       System.out.println("MIN_VALUE "+Character.MIN_VALUE);//

    }

}