/*Q29.Write a program to convert state of Double instance into byte, 
short, int, long, float and double.
*/
class Program29{
    public static void main1(String[] args){
        double num1=99.58;
        byte num2=(byte)num1;
        System.out.println("Num2 : "+num2);//99
    }

    public static void main2(String[] args){
        double num1=119.75;
        short num2=(short)num1;
        System.out.println("Num2 : "+num2);//119
    }

    public static void main3(String[] args){
        double num1=158.58;
        int num2=(int)num1;
        System.out.println("Num2 : "+num2);//158
    }

    public static void main4(String[] args){
        double num1=999.99;
        long num2=(long)num1;
        System.out.println("Num2 : "+num2);//999
    }

    public static void main5(String[] args){
        double num1=158.5834;
        float num2=(float)num1;
        System.out.println("Num2 : "+num2);//158.5834
    }

     public static void main(String[] args){
        double num1=998.4852;
        double num2=num1;
        System.out.println("Num2 : "+num2);//998.4852
    }

    
    


}
