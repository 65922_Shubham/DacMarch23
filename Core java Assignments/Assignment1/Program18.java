/*Q.18. Write a program to find minimum and maximum number as well as 
to add two integer numbers using methods of Integer.
*/
class Program18{
    public static void main(String[] args) {
        int a = 65;
        int b = 99;
        System.out.println(Integer.max(a, b)); 
        System.out.println(Integer.min(a, b)); 
        System.out.println(Integer.sum(a, b));
    }
}
