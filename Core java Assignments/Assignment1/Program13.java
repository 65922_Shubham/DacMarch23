/*Q13.Write a program to convert:
a. short value into String
b. short value into Short instance.
c. String instance into Short instance.
*/
class Program13{
    public static void main(String[] args){
        //a
        short s=777;
        String var=Short.toString(s);
        System.out.println("short value into String "+var);

        //b
        short value=889;
        Short ss=new Short(value);
        short num1=ss.shortValue();
        System.out.println("Num1 "+num1);

        //c
        String svar="999";
        Short cut=new Short(svar);
        System.out.println("String instance into Short instance "+cut);

    }
}
