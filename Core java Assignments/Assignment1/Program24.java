/*Q24.Write  a program to convert:
a. float value into String
b. float value into Float instance.
c. String instance into Float instance.
d. float value into hexadecimal string.
*/

class Program24{
    public static void main(String[] args){
        //a
        float f=88.88F;
        String fvar=Float.toString(f);
        System.out.println("float value into String "+fvar);

        //b
        float value=77.77F;
        Float fi = new Float(value);
        float num1=fi.floatValue();
        System.out.println("Num1 "+num1);

        //c
        String star="63.36";
        Float hvar=new Float(star);
        System.out.println("String instance into Float instance "+hvar);


        //d
        Float vi=96.24F;
        String svar=Float.toHexString(vi);
        System.out.println("float value into hexadecimal string "+svar);





    }
}