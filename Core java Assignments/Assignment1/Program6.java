/*Q6.Write a program to perform below operations on Boolean type to 
convert:
a. boolean value into String
b. boolean value into Boolean instance.
c. String value into boolean value
d. String value into Boolean instance.
*/
class Program6{
    public static void main(String[] args){
        //a
        boolean b=true;
        String bc=Boolean.toString(b);
        System.out.println("boolean value into String "+bc);

        //b
        boolean value=false;
        Boolean bd=new Boolean(value);
        boolean num1=bd.booleanValue();
        System.out.println("Num1 "+num1);

        //c
        boolean bg=Boolean.parseBoolean("true");
        System.out.println("String value into boolean value "+bg);
        
        //d
        String be="true";
        boolean yc=Boolean.valueOf(be);
        System.out.println("String value into Boolean instance "+yc);

    }
}