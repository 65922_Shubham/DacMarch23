/*Q28.Write a program to convert:
a. double value into String
b. double value into Double instance.
c. String instance into Double instance.
d. double value into binary, octal and hexadecimal 
string(Note: Here you can use doubleToLongBits() method 
along with methods of Long class).
*/

class Program28{
    public static void main(String[] args){
        //a
        double d=25.13;
        String dtar=Double.toString(d);
        System.out.println("double value into String "+dtar);
         //b
         double value=99.66;
         Double dvar=new Double(value);
         double num1=dvar.doubleValue();
        System.out.println("Num1  "+num1);

        //c
        String s="88.88";
        Double tar=new Double(s);
        System.out.println(" String instance into Double instance "+tar);

        //d
        double di=99.36;
        String svar=Double.toHexString(di);
        System.out.println("double value to hexadecimal string "+svar);

        

       





    }
}