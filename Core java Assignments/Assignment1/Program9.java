/*Q9.Write a program to convert state of Byte instance into byte, 
short, int. long, float and double.
*/
class Program9{
    public static void main1(String[] args){
        byte num1=10;
        byte num2=num1;
        System.out.println("Num2 "+num2);//10
    }
    public static void main2(String[] args){
        byte num1=20;
        //short num2=(short)num1;
        short num2=num1;
        System.out.println("Num2 "+num2);//10
    }
    public static void main3(String[] args){
        byte num1=30;
        //int num2=(int)num1;
        int num2=num1;
        System.out.println("Num2 "+num2);//30
    }
    public static void main4(String[] args){
        byte num1=40;
        //long num2=(long)num1;
        long num2=num1;
        System.out.println("Num2 "+num2);//40
    }
    public static void main5(String[] args){
        byte num1=50;
        //float num2=(float)num1;
        float num2=num1;
        System.out.println("Num2 "+num2);//50.0
    }

    public static void main(String[] args){
        byte num1=60;
        //double num2=(double)num1;
        double num2=num1;
        System.out.println("Num2 "+num2);//60.0
    }


}