/*Q25.Write a program to convert state of Float instance into byte, 
short, int, long, float and double.
*/

class Program25{
    public static void main1(String[] args){
        float num1=45.17F;
        byte num2=(byte)num1;
        System.out.println("Num2 : "+num2);//45
    }

    public static void main2(String[] args){
        float num1=179.74F;
        short num2=(short)num1;
        System.out.println("Num2 : "+num2);//179
    }

    public static void main3(String[] args){
        float num1=1200.00F;
        int num2=(int)num1;
        System.out.println("Num2 : "+num2);//1200
    }

    public static void main4(String[] args){
        float num1=881.17F;
        long num2=(long)num1;
        System.out.println("Num2 : "+num2);//881
    }

    public static void main5(String[] args){
        float num1=455.17F;
        float num2=num1;
        System.out.println("Num2 : "+num2);//455.17
    }

    public static void main(String[] args){
        float num1=199.25F;
        double num2=num1;
        System.out.println("Num2 : "+num2);//199.25
    }
}