/*Q20.Write a program to convert:
a. long value into String
b. long value into Long instance.
c. String instance into Long instance.
d. long value into binary, octal and hexadecimal string.
*/

class Program20{
    public static void main(String[] args){
        //a
        long l=888L;
        String lvar=Long.toString(l);
        System.out.println("long value into String "+lvar);

        //b
        long value=555L;
        Long ki=new Long(value);
        long num1=ki.longValue();
        System.out.println("Num1 "+num1);

        //c
        String qvar="666";
        Long svar=new Long(qvar);
        System.out.println("String instance into Long instance "+qvar);

        //d
        long lvar1=5555L;
        String ltar=Long.toBinaryString(lvar1);
        System.out.println("long value into Binary String "+ltar);
        String ltar1=Long.toOctalString(lvar1);
        System.out.println("long value into Octal String "+ltar1);
        String ltar2=Long.toHexString(lvar1);
        System.out.println("long value into Hexadecimal String "+ltar2);






    }
}
