/*Q17. Write a program to convert state of Integer instance into 
byte, short, int, long, float and double.
*/
class Program17{
    public static void main1(String[] args){
        int num1=129;
        byte num2=(byte)num1;
        System.out.println("Num2: "+num2);//-127
    }
    public static void main2(String[] args){
        int num1=130;
        short num2=(short)num1;
        System.out.println("Num2: "+num2);//130

    }
    public static void main3(String[] args){
        int num1=131;
        int num2=num1;
        System.out.println("Num2: "+num2);//131
    }
    public static void main4(String[] args){
        int num1=132;
        long num2=num1;
        System.out.println("Num2: "+num2);//132
    }
    public static void main5(String[] args){
        int num1=150;
        //float num2=(float)num1;
        float num2=num1;
        System.out.println("Num2: "+num2);//150.0
    }
    public static void main(String[] args){
        int num1=200;
        double num2=num1;
        System.out.println("Num2: "+num2);//200.0
    }
    
    
    

}