/*Q8.Write a program to convert:
a. byte value into String
b. byte value into Byte instance.
c. String instance into Byte instance.
*/


class Program8{
    public static void main(String[] args){
       //a
        byte a = 99;
        String str= Byte.toString(a);
        System.out.println("byte value into string "+str);

        //b
        byte value=100;
        Byte bs=new Byte(value);//Byte instance
        byte num1 = bs.byteValue();//instance method
        System.out.println("Num1 "+num1);
        
        //c
        String tpr="95";
        Byte cpo=new Byte(tpr);
        System.out.println(" String instance into Byte instance "+tpr);

    }
}