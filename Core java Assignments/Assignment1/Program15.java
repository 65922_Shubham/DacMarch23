/*Q15. Write a program to perform below operations on int type to 
get:
a. The number of bits used to represent a integer value
b. The number of bytes used to represent a integer value
c. The minimum value a integer
d. The maximum value a integer
*/
class Program15{
    public static void main(String[] args){
       System.out.println("SIZE "+Integer.SIZE);//32
       System.out.println("BYTES "+Integer.BYTES);//4
       System.out.println("MAX_VALUE "+Integer.MAX_VALUE);//2147483647
       System.out.println("MIN_VALUE "+Integer.MIN_VALUE);//

    }

}