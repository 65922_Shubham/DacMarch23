/*Q16.Write a program to convert:
a. int value into String
b. int value into Integer instance.
c. String instance into Integer instance.
d. int value into binary, octal and hexadecimal string.
*/

class Program16{
    public static void main(String[] args){
        //a
        int i=455;
        String ivar=Integer.toString(i);
        System.out.println("int value into String "+ivar);

        //b
        int value=696;
        Integer vi=new Integer(value);
        int num1=vi.intValue();
        System.out.println("Num1 "+num1);

        //c
        String ichar="444";
        Integer pp=new Integer(ichar);
        System.out.println("String instance into Integer instance "+pp);

        //d
        int ichar1=899;
        String itar=Integer.toBinaryString(ichar1);
        System.out.println("Int value into Binary String "+itar);
        String itar1=Integer.toOctalString(ichar1);
        System.out.println("Int value into Octal String "+itar1);
        String itar2=Integer.toHexString(ichar1);
        System.out.println("Int value into Hexadecimal String "+itar2);

    }
}