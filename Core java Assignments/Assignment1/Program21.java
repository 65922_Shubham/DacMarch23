/*Q21.Write a program to convert state of Long instance into byte, 
short, int, long, float and double
*/
class Program21{
    public static void main1(String[] args){
        long num1=130L;
        byte num2=(byte)num1;
        System.out.println("Num2 : "+num2);//-126
    }

    public static void main2(String[] args){
        long num1=40L;
        short num2=(short)num1;
        System.out.println("Num2 : "+num2);//40
    }

    public static void main3(String[] args){
        long num1=2500L;
        int num2=(int)num1;
        System.out.println("Num2 : "+num2);//2500
    }

    public static void main4(String[] args){
        long num1=900L;
        long num2=num1;
        System.out.println("Num2 : "+num2);//900
    }

    public static void main5(String[] args){
        long num1=700L;
        float num2=num1;
        System.out.println("Num2 : "+num2);//700.0
    }

    public static void main(String[] args){
        long num1=1200L;
        double num2=num1;
        System.out.println("Num2 : "+num2);//1200.0
    }
}