/*Q.30.Write a program to find minimum and maximum number as well as 
to add two double numbers using methods of Double.
*/



class Program30{
    public static void main(String[] args) {
        double a = 65.4032;
        double b = 89.9966;
        System.out.println(Double.max(a, b)); 
        System.out.println(Double.min(a, b)); 
        System.out.println(Double.sum(a, b));
    }
}