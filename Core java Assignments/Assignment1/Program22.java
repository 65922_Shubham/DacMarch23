/*Q.22. Write a program to find minimum and maximum number as well as 
to add two long numbers using methods of Long.
*/



class Program22{
    public static void main(String[] args) {
        long a = 555L;
        long b = 666L;
        System.out.println(Long.max(a, b)); 
        System.out.println(Long.min(a, b)); 
        System.out.println(Long.sum(a, b));
    }
}