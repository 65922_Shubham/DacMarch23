/*Q.7.Write a program to perform below operations on byte type to 
get:
a. The number of bits used to represent a byte value
b. The number of bytes used to represent a byte value
c. The minimum value a byte
d. The maximum value a byt
*/


class Program7{
    public static void main(String[] args){
       System.out.println("SIZE "+Byte.SIZE);//8
       System.out.println("BYTES "+Byte.BYTES);//1
       System.out.println("MAX_VALUE "+Byte.MAX_VALUE);//127
       System.out.println("MIN_VALUE "+Byte.MIN_VALUE);//-128

    }

}